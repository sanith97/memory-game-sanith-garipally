const mainContainer = document.getElementById("mainContainer");
const displayContainer = document.getElementById("game");
let playerScore = document.getElementById("displayPlayerScore");
let gameFinishContainer = document.getElementById("gameFinishContainer");
let guessWhoUrl =
  "https://image.shutterstock.com/image-vector/guess-who-retro-speech-bubble-260nw-428161042.jpg";
let homepageScoreEle = document.getElementById("homepageScore");

if(localStorage.getItem("bestScore") == null) {
  localStorage.setItem("bestScore", JSON.stringify(0));
}

homepageScoreEle.textContent = JSON.parse(localStorage.getItem("bestScore"));
let userGameScoreEle = document.getElementById("userGameScore");

let startPageElement = document.getElementById("startPage");
let startGameBtn = document.getElementById("startGame");
startGameBtn.addEventListener("click", () => {
  startPageElement.classList.add("displayControl");
  mainContainer.classList.remove("displayControl");
});

let playAgainBtn = document.getElementById("playAgain");
  playAgainBtn.addEventListener("click", () => {
  homepageScoreEle.textContent = JSON.parse(localStorage.getItem("bestScore"));
  gameFinishContainer.classList.add("displayControl");
  startPageElement.classList.remove("displayControl");
  location.reload();
});

const gifArray = [
  {
    name: "cat",
    image: "gifs/1.gif",
  },
  {
    name: "simpson",
    image: "gifs/2.gif",
  },
  {
    name: "minions",
    image: "gifs/3.gif",
  },
  {
    name: "crazyfrog",
    image: "gifs/4.gif",
  },
  {
    name: "Obama",
    image: "gifs/5.gif",
  },
  {
    name: "batman",
    image: "gifs/6.gif",
  },
  {
    name: "seaturtle",
    image: "gifs/7.gif",
  },
  {
    name: "rabbit",
    image: "gifs/8.gif",
  },
  {
    name: "misssbest",
    image: "gifs/9.gif",
  },
  {
    name: "popsinger",
    image: "gifs/10.gif",
  },
  {
    name: "supermario",
    image: "gifs/11.gif",
  },
  {
    name: "astonishing",
    image: "gifs/12.gif",
  },
  {
    name: "cat",
    image: "gifs/1.gif",
  },
  {
    name: "simpson",
    image: "gifs/2.gif",
  },
  {
    name: "minions",
    image: "gifs/3.gif",
  },
  {
    name: "crazyfrog",
    image: "gifs/4.gif",
  },
  {
    name: "Obama",
    image: "gifs/5.gif",
  },
  {
    name: "batman",
    image: "gifs/6.gif",
  },
  {
    name: "seaturtle",
    image: "gifs/7.gif",
  },
  {
    name: "rabbit",
    image: "gifs/8.gif",
  },
  {
    name: "misssbest",
    image: "gifs/9.gif",
  },
  {
    name: "popsinger",
    image: "gifs/10.gif",
  },
  {
    name: "supermario",
    image: "gifs/11.gif",
  },
  {
    name: "astonishing",
    image: "gifs/12.gif",
  },
];

//simple sorting method
gifArray.sort(() => {
  return 0.5 - Math.random();
});

let gifsClickedByUser = [];
let userClickedGifId = [];
let selectedGifElements = [];

let userClicksCount = 0;
let winCount = 0;

//function creates the elements including a score container
function createGame(initialValue, lengthValue) {
  for (let index = initialValue; index < lengthValue; index++) {
    let gifContainer = document.createElement("div");
    let gif = document.createElement("img");
    gif.addEventListener("click", addGifToGame);
    gif.setAttribute("src", gifArray[index]['image']);
    gif.setAttribute("id", index);
    gif.classList.add("opacityControl")
    gifContainer.style.backgroundImage = "url('https://image.shutterstock.com/image-vector/guess-who-retro-speech-bubble-260nw-428161042.jpg')";
    gifContainer.style.backgroundSize = "cover";
    gifContainer.appendChild(gif);
    displayContainer.appendChild(gifContainer);
  }
}

//creating first Set
createGame(0, gifArray.length / 2);

//creating second set
createGame(gifArray.length / 2, gifArray.length);

//function checks whether gifs clicked by the user, same or not
function isGifsMatched() {
  if (winCount < gifArray.length / 2 ) {
    if (gifsClickedByUser[0] == gifsClickedByUser[1] && (userClickedGifId[0] != userClickedGifId[1])) { //if same image is clicked twice it will not enter into conditional block
      winCount += 1;
      document
        .getElementById([userClickedGifId[0]])
        .removeEventListener("click", addGifToGame);
      document
        .getElementById([userClickedGifId[1]])
        .removeEventListener("click", addGifToGame);

      gifsClickedByUser = [];
      userClickedGifId = [];
      selectedGifElements = [];

    }
    else {
      setTimeout(() => {
        // selectedGifElements[0].setAttribute("src", guessWhoUrl);
        // selectedGifElements[1].setAttribute("src", guessWhoUrl);
        selectedGifElements.forEach((element) => {
          element.classList.add("opacityControl");
        })
        gifsClickedByUser = [];
        userClickedGifId = [];
        selectedGifElements = [];
      }, 400);
    }

    if(winCount == gifArray.length/2) {
      console.log("inside local storage setting")
      
      if(JSON.parse(localStorage.getItem("bestScore")) == 0) {
        localStorage.setItem("bestScore", JSON.stringify(userClicksCount));
      }
        //only if the clicks are lesser than the stored value, then rewriting the stored value
      if (JSON.parse(localStorage.getItem("bestScore")) > userClicksCount) {
        localStorage.setItem("bestScore", JSON.stringify(userClicksCount));

      }

      homepageScoreEle.textContent = JSON.parse(localStorage.getItem("bestScore"));
      userGameScoreEle.textContent = userClicksCount;
      mainContainer.classList.add("displayControl");
      gameFinishContainer.classList.remove("displayControl");
    }
  }
}

//callback function added to event listener
function addGifToGame(event) {
  userClicksCount += 1;
  playerScore.textContent = userClicksCount;
  let gifId = this.getAttribute("id");
  gifsClickedByUser.push(gifArray[gifId]["name"]);
  userClickedGifId.push(gifId);
  selectedGifElements.push(event.target);
  
  this.classList.remove("opacityControl");  

  if (userClickedGifId.length == 2) {
    // setTimeout(isGifsMatched, 700);
    isGifsMatched()

  } 
  
}
